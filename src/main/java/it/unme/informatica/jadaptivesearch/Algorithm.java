/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.it.unme.informatica.jadaptivesearch;

import java.util.Arrays;

/**
 *
 * @author Francesco Pagano
 */
public class Algorithm {

    private static int itera = 0;
    private TestSet whereFind,  whatFind;
    static public final int BINARY_SEARCH = 0;
    static public final int ADAPTIVE_SEARCH = 1;

    public Algorithm(TestSet whereFind, TestSet whatFind) {
        this.whereFind = whereFind;
        this.whatFind = whatFind;
    }

    public long elapsedTime(int numAlg) {
        // Get the start time of the process
        long start = System.nanoTime();
//        System.out.println("Start: " + start);

        switch (numAlg) {
            case BINARY_SEARCH:
                binarySearch();
                break;
            case ADAPTIVE_SEARCH:
                adaptiveSearch();
                break;
        }

        // Get the end time of the process
        long end = System.nanoTime();
//        System.out.println("End  : " + end);

        long elapsedTime = end - start;

        // Show how long it took to finish the process
        return elapsedTime;
    }

    private void adaptiveSearch() {
        for (double item : whatFind.getDataSet()) {
            int posFound = adaptiveSearchImpl(whereFind.getDataSet(), item);
//          int posFound = oldSearch(whereFind.getDataSet(), item);
        }
    }

    private void binarySearch() {
        for (double item : whatFind.getDataSet()) {
            int posFound = Arrays.binarySearch(whereFind.getDataSet(), item);
        }
    }

    public void testResultSearch() {
        int diversi = 0, pos = 0, pos2 = 0, pos3 = 0;
        for (double item : whatFind.getDataSet()) {
            int pos1 = Arrays.binarySearch(whereFind.getDataSet(), item);
            System.out.println("***** " + (++pos) + ") pos binary:" + pos1);
            pos2 = adaptiveSearchImpl(whereFind.getDataSet(), item);

            if (pos1 >= 0) {
                if (pos1 != pos2) {
                    System.out.println("Serch key:" + item + " pos binary:" + pos1 + " pos AdaptiveSearch:" + pos2);
                    diversi++;
                }
            }
        }
        System.out.println("Number of keys found at different places:" + diversi);
    }

    int oldSearch(double a[], double key) {
        System.out.println("--- oldSearch: start ---");
        int n = a.length;
        int bot = 0, top = n - 1, next, med, nontrovato = 1;
        while ((a[top] >= key) && (a[bot] < key)) {
            med = (top - bot) / 2;//+ 1;
            next = (int) (bot + (top - bot) * (key - a[bot]) / (a[top] - a[bot]));
//            System.out.println("- bottom:" + bot + " top:" + top + " med:" + med + " next:" + next);
            if ((key < a[next]) && (next - bot) > med) {
                top = next - 1;
                next = (bot + top) / 2;
            } else if ((key > a[next]) && (top - next) > med) {
                bot = next + 1;
                next = (bot + top) / 2;
            }
            if (key < a[next]) {
                top = next - 1;
            } else if (key > a[next]) {
                bot = next + 1;
            } else {
                bot = next;
            }
        }
        System.out.println("---- oldSearch: end ---");
        if (a[bot] == key) {
            return bot;
        } else {
            return -bot;
        }
    }

    public static int adaptiveSearchImpl(double a[], double key) {
        int bot, top, next, med, position;
        boolean finished = false;
        System.out.println("--- AdaptiveSearch: begin ---");
//        itera++;
//        System.out.println("****** iteration:" + itera + " searching for:" + key);
        bot = 0;
        top = a.length - 1;
        position = -1;
        while ((position == -1) && (bot < top)) {
//        while ((a[top] >= key) && (a[bot] < key)) {

            med = (top - bot) / 2;

//find the middle point for interpolation
            next = (int) ((key - a[bot]) / (a[top] - a[bot]) * (top - bot) + bot);
            System.out.println("- bottom:" + bot + " top:" + top + " med:" + med + " next:" + next);
            System.out.println("  a[bottom]:" + a[bot] + " a[top]:" + a[top] + " a[med]:" + a[med] + " a[next]:" + a[next]);

//if the size of the interval position through interpolation is larger of the half of the segment then we halve it
            if ((key < a[next]) && ((next - bot) > med)) {
                top = next - 1;
                next = (bot + top) / 2;
            } else if ((key > a[next]) && ((top - next) > med)) {
                bot = next + 1;
                next = (bot + top) / 2;
            }

//verify key position and halve the search interval
            if (key > a[next]) {
                bot = next + 1;
            } else if (key < a[next]) {
                top = next - 1;
            } else {
                position = next;
            }
        }

//if the key is position in either of the two ends then return its position, else return 0 (not position)
        /*if (a[bot] == key) {
        position = bot;
        } else */ if (a[top] == key) {
            position = top;
        } else {
            position = -top - 1;
        }
        System.out.println("--- AdaptiveSearch: end ---");

        return position;
    }
}
