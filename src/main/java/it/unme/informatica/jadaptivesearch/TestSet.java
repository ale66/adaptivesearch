/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.it.unme.informatica.jadaptivesearch;

import java.util.Arrays;

/**
 *
 * @author Francesco
 */
public class TestSet {

    private long maxItem = 100000000L;    // the max value for item
    private int dimDataSet = 100000;  // how many integers we want to test
    private double dataSet[] = new double[dimDataSet];

    public TestSet() {
        init(null,true);
    }

    public TestSet(int dimDataSet) {
        setDataSet(dataSet);
        init(null,true);
    }

    public TestSet(double source[],int dimDataSet, boolean ordered) {
        setDataSet(dataSet);
        init(source,ordered);
    }

    private void init(double source[],boolean ordered) {
        maxItem=10*dimDataSet;
        populate(source);
        if(ordered)
            order();
    }

    /**
     * @return the dataSet
     */
    public double[] getDataSet() {
        return dataSet;
    }

    /**
     * @param dataSet the dataSet to set
     */
    public void setDataSet(double[] dataSet) {
        this.dataSet = dataSet;
    }

    private void order() {
        Arrays.sort(this.dataSet);

    }

    private void populate(double source[]) {
        int pos = 0;
        for (; source!=null && pos < dataSet.length/2; pos++) {
            dataSet[pos] = source[(int) (Math.floor(Math.random() * source.length))];   // prendo un valore a caso da source
        }
        for (; pos < dataSet.length; pos++) {
            dataSet[pos] = Math.random() * 2 * maxItem - maxItem;  // a number between (-MAX_ITEM,MAX_ITEM)
        }
    }

    public void print() {
        System.out.println("***** "+this+" *****");
        for (double item : dataSet) {
            System.out.print(item+"  ");
        }
        System.out.println("\n***** end *****");
    }
}
