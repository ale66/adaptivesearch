/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.it.unme.informatica.jadaptivesearch;

/**
 *
 * @author Francesco
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestSet whereFind = new TestSet(10000000);
//        whereFind.print();
        TestSet whatFind = new TestSet(whereFind.getDataSet(), 10000, false);
//        whatFind.print();
        Algorithm alg = new Algorithm(whereFind, whatFind);
        alg.testResultSearch();
//        System.out.println("***** elapsed time for binary search:   " + alg.elapsedTime(Algorithm.BINARY_SEARCH) + " nanosec");
//        System.out.println("***** elapsed time for adaptive search: " + alg.elapsedTime(Algorithm.ADAPTIVE_SEARCH) + " nanosec");
    }
}
