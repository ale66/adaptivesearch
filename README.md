# README #

JAdaptiveSearch, written by [Francesco Pagano](https://scholar.google.com/citations?hl=en&user=_Dl0BlEAAAAJ&view_op=list_works&sortby=pubdate), is a Java implementation of a benchmark for comparing the speed and memory access costs of the new Adaptive Search algorithm.

AdaptiveSearch is an algorithm for search /membership queries over ordered sets.

Please refer to the article for the details:


_Adaptive search over sorted sets._

B. Bonasera, E. Ferrara, G. Fiumara, F. Pagano and A. Provetti, 2015.

Journal of Discrete Algorithms, Vol. 30, pp. 128-133.

DOI: [http://dx.doi.org/10.1016/j.jda.2014.12.007](http://dx.doi.org/10.1016/j.jda.2014.12.007).

Reviewed on MatSciNet: [http://www.ams.org/mathscinet-getitem?mr=3305156](http://www.ams.org/mathscinet-getitem?mr=3305156).

### Contribution guidelines ###

* Please contact us before starting.

### Who do I talk to? ###

* [Alessando Provetti](http://www.dcs.bbk.ac.uk/~ale/) is currently managing the repository.

* Thanks to Salvatore Rapisarda for help with Apache Maven.